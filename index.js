// console.log('testing s33 a1');

const url = 'https://jsonplaceholder.typicode.com'


// #4
fetch(`${url}/todos`)
.then(res => res.json())
.then(array => console.log(array.map(item => item.title)));


// #5-6
fetch(`${url}/todos/1`)
.then(res => res.json())
.then(object => console.log(`Thie item "${object.title}" on the list has a status of ${object.completed}`))

// #7
fetch(`${url}/todos`, {
	method: 'POST',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
	"userId": 5,
    "title": "Created To Do List Item",
    "completed": false
	})
})
.then(res => res.json())
.then(object => console.log(object))


// #8-9
fetch(`${url}/todos/1`, {
	method: 'PUT',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
	"userId": 1,
    "title": "Updated To Do List Item",
    "description" : "To update the my to do list with a different data structure.",
    "status": "Completed",
    "dateCompleted" : "Pending"
	})
})
.then(res => res.json())
.then(object => console.log(object))


// #10-11
fetch(`${url}/todos/1`, {
	method: 'PATCH',
	headers: {
		'Content-type' : 'application/json'
	},
	body: JSON.stringify({
    "status": "Complete",
    "dateCompleted": "07/09/21"
	})
})
.then(res => res.json())
.then(object => console.log(object))


// #12
fetch(`${url}/todos/3`, {
	method: 'DELETE',
})
.then(res => res.json())
.then(object => console.log(object))